﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPyramid : MonoBehaviour
{

    public int pyramidLevels = 10;
    public GameObject objectToSpawn;
    public int margin = 1;
    private int oddNumber = 1;
    private Vector3 position;
    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position;

        for (int i = 0; i < pyramidLevels; i++)
        {
            int borders = (oddNumber - 1) / 2;
            for (int j = -borders; j <= borders; j++)
            {
                for (int k = -borders; k <= borders; k++)
                {
                    position = new Vector3(margin * j, -margin * i, margin * k) + offset;
                    Instantiate(objectToSpawn, position, Quaternion.identity);
                }
            }
            oddNumber += 2;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
