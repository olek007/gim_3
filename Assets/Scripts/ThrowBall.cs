﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBall : MonoBehaviour {

    public GameObject objectToThrow;
    public int force = 10000;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
           
            GameObject temp = Instantiate(objectToThrow, transform);
            temp.GetComponent<Rigidbody>().AddForce(ray.direction * force);
        }
    }
}
